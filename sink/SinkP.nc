#include "myMessage.h"

module SinkP {

	uses {
		interface Boot;
		interface Leds;
		interface AMSend as SerialSend;
		interface AMSend as RadioSend;
		interface Receive as RadioReceive;
		interface Timer<TMilli> as SenseDataTimer;
		interface Packet;
		interface Packet as RadioPacket;
		interface SplitControl as RadioControl;
		interface SplitControl as SerialControl;

		interface Read<uint16_t> as Temperature;
		interface Read<uint16_t> as Humidity;
		interface Read<uint16_t> as Light;
	}
}

implementation {

	message_t pkt;

	bool RadioBusy = FALSE;
	bool SerialBusy = FALSE;

	uint16_t temperature;
	uint16_t humidity;
	uint16_t light;

	uint16_t seq_num = 0;

	event void Boot.booted() {
		call RadioControl.start();
		call SerialControl.start();
	}

	event void RadioControl.startDone(error_t error) {
		if(error != SUCCESS) {
			call RadioControl.start();
		}
	}

	event void SerialControl.startDone(error_t error) {
		if(error != SUCCESS) {
			call SerialControl.start();
		} else {
			call SenseDataTimer.startPeriodic(SENSE_DATA_FREQUENCY);
		}
	}

	event void SenseDataTimer.fired() {

		if(!SerialBusy) {
		
			myMessage* toBeSentMsg = (myMessage*) call Packet.getPayload(&pkt, sizeof(myMessage));

			call Leds.led0Toggle();
			
			call Temperature.read();
			call Humidity.read();
			call Light.read();

			toBeSentMsg->type = DATA_MESSAGE;
			toBeSentMsg->nodeid = TOS_NODE_ID;
			toBeSentMsg->temperature = temperature;
			toBeSentMsg->humidity = humidity;
			toBeSentMsg->light = light;
			toBeSentMsg->seq_num = seq_num;

			toBeSentMsg->tot_node = 1;
			toBeSentMsg->path[1] = TOS_NODE_ID;

			++ seq_num;

			if((call SerialSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
				SerialBusy = TRUE;
			}
		}
	}

	event void Temperature.readDone(error_t error, uint16_t data) {
		temperature = data;
	}

	event void Humidity.readDone(error_t error, uint16_t data) {
		humidity = data;
	}

	event void Light.readDone(error_t error, uint16_t data) {
		light = data;
	}

	event void RadioControl.stopDone(error_t error) {
		// do nothing
	}

	event void SerialControl.stopDone(error_t error) {
		// do nothing
	}

	event message_t* RadioReceive.receive(message_t* msg, void* payload, uint8_t len) {

		myMessage* receivedMsg = (myMessage*) payload;

		if(receivedMsg->dest != TOS_NODE_ID && receivedMsg->dest != 0) {
			return msg;
		}

		if(receivedMsg->nodeid == TOS_NODE_ID) {
			return msg;
		}

		if(len != sizeof(myMessage)) {
			return msg;
		}

		if(receivedMsg->nodeid != TOS_NODE_ID) {

			if(receivedMsg->type == DATA_MESSAGE) {

				if(!SerialBusy) {
			
					myMessage* toBeSentMsg = (myMessage*) call Packet.getPayload(&pkt, sizeof(myMessage));

					int i;

					call Leds.led2Toggle();

					toBeSentMsg->nodeid = receivedMsg->nodeid;
					toBeSentMsg->temperature = receivedMsg->temperature;
					toBeSentMsg->humidity = receivedMsg->humidity;
					toBeSentMsg->light = receivedMsg->light;
					toBeSentMsg->seq_num = receivedMsg->seq_num;

					toBeSentMsg->tot_node = receivedMsg->tot_node + 1;
					for(i=1; i<toBeSentMsg->tot_node; ++i) {
						toBeSentMsg->path[i] = receivedMsg->path[i];
					}
					toBeSentMsg->path[toBeSentMsg->tot_node] = TOS_NODE_ID;

					if((call SerialSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
						SerialBusy = TRUE;
					}
				}

			} else if(receivedMsg->type == SENSE_MESSAGE) {

				if(!RadioBusy) {
				
					myMessage* toBeSentMsg = (myMessage*) call RadioPacket.getPayload(&pkt, sizeof(myMessage));
					
					call Leds.led1Toggle();

					toBeSentMsg->type = REPLY_MESSAGE;
					toBeSentMsg->nodeid = TOS_NODE_ID;
					toBeSentMsg->dv_dist = 0;

					toBeSentMsg->dest = receivedMsg->nodeid;

					if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
						RadioBusy = TRUE;
					}
				}
			} else {
				// if receiving a control message, do nothing since distance between sink to sink is always 0
				// if receiving a reply messge, do nothing since sink never send out sense message
			}
		}

		return msg;
	}

	event void RadioSend.sendDone(message_t* msg, error_t error) {
	
		if(msg == &pkt) {

			call Leds.led1Off();

			RadioBusy = FALSE;
		}
	}

	event void SerialSend.sendDone(message_t* msg, error_t error) {

		if(msg == &pkt) {

			call Leds.led0Off();
			call Leds.led2Off();

			SerialBusy = FALSE;
		}
	}
}
