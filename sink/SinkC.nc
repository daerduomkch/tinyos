#include "myMessage.h"

configuration SinkC {

}

implementation {

	components MainC, LedsC;
	components SinkP as App;
	components new TimerMilliC() as Timer;
	components ActiveMessageC as Radio;
	components SerialActiveMessageC as Serial;
	components new SensirionSht11C() as SHT11C;
	components new HamamatsuS1087ParC() as LightC;

	App.Boot -> MainC;
	App.Leds -> LedsC;
	App.SenseDataTimer -> Timer.Timer;
	App.SerialSend -> Serial.AMSend[AM_MYMESSAGE];
	App.RadioSend -> Radio.AMSend[AM_MYMESSAGE];
	App.RadioReceive -> Radio.Receive[AM_MYMESSAGE];
	App.Packet -> Serial;
	App.RadioPacket -> Radio.Packet;
	App.RadioControl -> Radio.SplitControl;
	App.SerialControl -> Serial.SplitControl;

	App.Temperature -> SHT11C.Temperature;
	App.Humidity -> SHT11C.Humidity;
	App.Light -> LightC;
}
