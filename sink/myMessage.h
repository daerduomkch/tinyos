#ifndef MYMESSAGE_H
#define MYMESSAGE_H

enum {
	SENSE_DATA_FREQUENCY = 3000,
	
	AM_MYMESSAGE = 10,
	MAX_NODE_NUMBER = 10,
	MAX_UINT8_INT = 127,
	SINK_NODE_ID = 1,

	// types of message
	DATA_MESSAGE,
	CONTROL_MESSAGE,
	SENSE_MESSAGE,
	REPLY_MESSAGE
};

typedef nx_struct myMessage{

	// type of message
	nx_uint8_t type;
	// destination nodeid
	nx_uint8_t dest;

	// data message
	nx_uint16_t nodeid;
	nx_uint16_t temperature;
	nx_uint16_t humidity;
	nx_uint16_t light;
	nx_uint16_t seq_num;

	// control message
	nx_uint8_t dv_dist;

	// only for debug
	nx_uint8_t path[MAX_NODE_NUMBER];
	nx_uint8_t tot_node;
	
} myMessage;

#endif
