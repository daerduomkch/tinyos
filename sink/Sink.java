import java.io.IOException;

import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class Sink implements MessageListener {

	private MoteIF moteIF;
	private MFrame mFrame;

	public Sink(MoteIF moteIF) {
		mFrame = new MFrame();
		this.moteIF = moteIF;
		this.moteIF.registerListener(new myMessage(), this);
	}

  	public void messageReceived(int to, Message message) {
		System.out.println("hello");
    	myMessage msg = (myMessage) message;
			mFrame.update(msg.get_nodeid(), new java.util.Date(), msg.get_temperature(), msg.get_humidity(), msg.get_light());
		mFrame.draw();
    	System.out.print("Received packet: nodeid = " + msg.get_nodeid() + 
    					", seq_num = " + msg.get_seq_num() +
    					", temperature = " + msg.get_temperature() + 
    					", humidity = " + msg.get_humidity() +
    					". light = " + msg.get_light() + "\n \t\t path: ");
  	
	for(int i=1; i<msg.get_tot_node(); ++i) {
		System.out.print(msg.getElement_path(i) + " -> ");
	}
	System.out.println(msg.getElement_path(msg.get_tot_node()) + "\n");
	
	}
  
  	private static void usage() {
    	System.err.println("usage: TestSerial [-comm <source>]");
  	}
  
  	public static void main(String[] args) throws Exception {
    	String source = null;
    	if (args.length == 2) {
      		if (!args[0].equals("-comm")) {
				usage();
				System.exit(1);
      		}
      		source = args[1];
    	}
    	else if (args.length != 0) {
      		usage();
      		System.exit(1);
    	}
    
    	PhoenixSource phoenix;
    
    	if (source == null) {
      		phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
    	}
    	else {
      		phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
    	}

    	MoteIF mif = new MoteIF(phoenix);
    	Sink serial = new Sink(mif);
  	}
}
