import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;


public class MFrame{	
				private class MyMouseAdaptor extends MouseAdapter implements MouseListener{
							int chosenSensor = -1;
							int chosenProbe = -1;
							MFrame chosenFrame = null;
							
							public MyMouseAdaptor(MFrame f, int sensor, int probe){
										super();
										chosenFrame = f;
										chosenSensor = sensor;
										chosenProbe = probe;
							}

							@Override
							public void mouseClicked(MouseEvent evt){
										if (chosenSensor==-1) chosenFrame.chosenProbe = chosenProbe;
										else if (chosenProbe==-1) chosenFrame.chosenSensor = chosenSensor;
										else assert(false);
										System.out.println("Change to "+chosenFrame.chosenSensor + " " + chosenFrame.chosenProbe);
										chosenFrame.draw();
							}
				}
	
				private class SensingData {
					public int temp, humid, light;
					public java.util.Date seqnum;
					public int sensorNum;
					SensingData(java.util.Date s, int t, int h, int l){
							seqnum = s;		temp = t;    humid = h;    light = l;
					}
					public int getValue(int i){
							if (i==0) return temp;
							else if (i==1) return humid;
							else if (i==2) return light;
							else return -1;
					}
				}
				
				private TimeSeriesCollection createDataset(){
					/*
				    DefaultCategoryDataset dataset = new DefaultCategoryDataset();

							for (int i=0;i<SensorNum;++i){		
							
							 			long lastSeqNum = -1;
							 			while (it.hasNext()){
							 						SensingData tmpData = it.next();
							 						tmpData.sensorNum = SensorList[i];
							 						if (tmpData.seqnum!=lastSeqNum) tempQueue.add(tmpData);
							 						lastSeqNum = tmpData.seqnum;
							 			}
							 			
							 			while (!tempQueue.isEmpty()){
				 									SensingData tmpData = tempQueue.poll();
		 											dataset.addValue(tmpData.getValue(chosenProbe), "Sensor "+tmpData.sensorNum, new Long(tmpData.seqnum));
	 												System.out.println("ADD "+ tmpData.getValue(chosenProbe) + " " + tmpData.sensorNum + " " + tmpData.seqnum);
				 						}
							}
				    
				    return dataset;*/
					
					 TimeSeriesCollection timeseriescolection=new TimeSeriesCollection();
					 for (int i=0;i<SensorNum;++i){
						 			TimeSeries tmpSeries = new TimeSeries("Sensor "+SensorList[i], Millisecond.class);
						 			while ( (!sensorQueue[i].isEmpty()) && (sensorQueue[i].peek().seqnum.getTime()+20000<currentSeqNum.getTime())) sensorQueue[i].poll();
						 			Iterator<SensingData> it = sensorQueue[i].iterator();
						 			while (it.hasNext()){
						 					SensingData tmpData = it.next();
						 					tmpSeries.add(new Millisecond(tmpData.seqnum), tmpData.getValue(chosenProbe));
						 			}
								  timeseriescolection.addSeries(tmpSeries);
					 	}
					 return timeseriescolection;
				}
				
				public void draw(){
								XYDataset dataset = createDataset();
								JFreeChart jfreechart; 
								if (chosenProbe>=0) jfreechart = ChartFactory.createTimeSeriesChart(ProbeName[chosenProbe]+" 实时折线图" , "sequence num", "value", dataset, true, true, false);  
								else jfreechart = ChartFactory.createTimeSeriesChart(" 实时折线图" , "sequence num", "value", dataset, true, true, false);  
								// XYPlot plot = (XYPlot) jfreechart.getXYPlot();
								// NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
								// yAxis.setLowerBound(LowerBound[chosenProbe]);
								 
								 
			        ChartPanel chartFrame=new ChartPanel(jfreechart,true);
			        
			        /*draw the chart on the*/
			 					c.gridx = 0;
			 					c.gridy = 2;
			 					c.gridwidth = GridBagConstraints.REMAINDER;
			 					layout.setConstraints(chartFrame, c);
			 					frame.getContentPane().add(chartFrame);
						 		frame.pack();
						 		frame.setVisible(true);
				}
				
				public void update(int sNum, java.util.Date seqNum, int temp, int humid, int light){
								int i;
								for (i=0;i<SensorNum;++i)
										if (SensorList[i] == sNum) break;
								if (i==SensorNum) {
									System.out.println("Error: Node " +sNum+ " not found!");
									return;
								}
								else System.out.println("Sensor "+SensorList[i]+" update");
								sensorQueue[i].add(new SensingData(seqNum, temp, humid, light));
								if (seqNum.compareTo(currentSeqNum)>0) currentSeqNum = seqNum;
				}
				
			 final static int SensorNum = 5;
			 final static int SensorList[] = {1, 2, 3, 4, 5};
			 final static int ProbeNum = 3;
			 final static String ProbeName[] = {"Temperature", "Humidity", "Light"};
			 final static double LowerBound[]={0,0,0};
			 
			 final static int WindowSize = 20000;
			 
		 	 GridBagLayout layout = new GridBagLayout();
		 	 GridBagConstraints c = new GridBagConstraints();
		 	 JFrame frame = new JFrame();
		 	 
			 int chosenSensor = -1;
			 int chosenProbe = -1;
			 java.util.Date currentSeqNum = new java.util.Date();
			 PriorityQueue<SensingData>[] sensorQueue;
			 
			public MFrame(){
					// create data buffer
					 sensorQueue = new PriorityQueue[SensorNum];
					 for (int i=0;i<SensorNum;++i)
						 sensorQueue[i] = new PriorityQueue<SensingData>(11,  
					                new Comparator<SensingData>() {  
					                  public int compare(SensingData s1, SensingData s2) {  
					                  						return s1.seqnum.compareTo(s2.seqnum);}});
				
				 		// set layout type
					 c.fill = GridBagConstraints.NONE;
				 		c.anchor = GridBagConstraints.WEST;
				 		c.insets = new Insets(5,10,5,10);
 				
				 		// set frame type
				 		frame.getContentPane().setLayout(layout);
     	
				 		// add basic button to the frame
				 		JLabel probeLabel = new JLabel("Probe List:");
				 		c.gridx = 0;
				 		c.gridy = 0;
				 		layout.setConstraints(probeLabel, c);
				 		frame.getContentPane().add(probeLabel);
     				
				 		JButton []probeButton = new JButton[ProbeNum];
				 		for (int i=0;i<ProbeNum;++i){
				 				probeButton[i] = new JButton(ProbeName[i]);
				 				probeButton[i].addMouseListener(new MyMouseAdaptor(this, -1, i));
				 				probeButton[i].setFont(new Font("Helvetica",Font.PLAIN, 14));
				 				c.gridx = i+1;
				 				c.gridy = 0;
				 				layout.setConstraints(probeButton[i], c);
				 				frame.getContentPane().add(probeButton[i]);			
				 		}

				     	/*
				 		JLabel sensorLabel = new JLabel("Sensor List:");
				 		c.gridx = 0;
				 		c.gridy = 1;
				 		layout.setConstraints(sensorLabel, c);
				 		frame.getContentPane().add(sensorLabel);
     	
				 		JButton []SensorButton = new JButton[SensorNum];
				 		for (int i=0;i<SensorNum;++i){
				 				SensorButton[i] = new JButton("sensor "+sensorList[i]);
				 				SensorButton[i].addMouseListener(new MyMouseAdaptor(this, i, -1));
				 				SensorButton[i].setFont(new Font("Helvetica",Font.PLAIN, 14));
				 				c.gridx = i+1;
				 				c.gridy = 1;
				 				layout.setConstraints(SensorButton[i], c);
				 				frame.getContentPane().add(SensorButton[i]);			
				 		}
				 		*/
				 		
				 		// show the frame on screen
				 		frame.pack();
				 		frame.setVisible(true);
				 		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				 		
			 	}
}
