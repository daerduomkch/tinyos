#include <Timer.h>
#include "myMessage.h"

module SenserP {

	uses {
		interface Boot;
		interface Leds;
		interface Timer<TMilli> as SenseDataTimer;
		interface Timer<TMilli> as TimeoutTimer;
		interface Timer<TMilli> as SenseNeighborTimer;
		interface SplitControl as RadioControl;
		interface Packet;
		interface AMSend as RadioSend;
		
		interface Read<uint16_t> as Temperature;
		interface Read<uint16_t> as Humidity;
		interface Read<uint16_t> as Light;

		// to implement multi-hop
		interface Receive as RadioReceive;
	}
}

implementation {

	message_t pkt;

	bool busy = FALSE;

	uint16_t temperature;
	uint16_t humidity;
	uint16_t light;

	uint16_t seq_num = 0;

	/* simplified distance vector routing algorithm*/
	uint8_t dv_dist[MAX_NODE_NUMBER];		// dv_dist[i]=j represents that from i to sink need j hops
	uint8_t min_dist = MAX_UINT8_INT;		// the current minimum distance to sink
	uint8_t min_node = 1;					// the neighbor through which gives the path of the minimum distance
	bool dv_neighbor[MAX_NODE_NUMBER]; 		// indicate all current neighbors
	uint8_t dv_tot_neighbor = 0;			// total number of neightbors

	bool dv_neighbor_t[MAX_NODE_NUMBER];	// used when sensing neighbors
	bool SensingNeighbor = FALSE;

	event void Boot.booted() {
		call RadioControl.start();
	}

	event void RadioControl.startDone(error_t error) {

		if(error == SUCCESS) {

			int i;

			// distance vector routing algorithm
			for(i=1; i<=MAX_NODE_NUMBER; ++i) {
				dv_dist[i] = MAX_UINT8_INT;
			}

			call SenseNeighborTimer.startPeriodic(SENSE_NEIGHBOR_FREQUENCY);
			call SenseDataTimer.startPeriodic(SENSE_DATA_FREQUENCY);

		} else {
			call RadioControl.start();
		}
	}

	event void SenseNeighborTimer.fired() {

		if(!busy) {

			myMessage* msg = (myMessage *) call Packet.getPayload(&pkt, sizeof(myMessage));
				
			call TimeoutTimer.startOneShot(TIMEOUT_LENGTH);

			call Leds.led1Toggle();

			msg->type = SENSE_MESSAGE;
			msg->nodeid = TOS_NODE_ID;
			msg->dest = 0;

			if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {

				int i;

				busy = TRUE;
				SensingNeighbor = TRUE;

				for(i=1; i<=MAX_NODE_NUMBER; ++i) {
					dv_neighbor_t[i] = dv_neighbor[i];
				}
			}
		}
	}

	void SendControlMessage() {
		
		if(!busy) {

			myMessage* msg = (myMessage *) call Packet.getPayload(&pkt, sizeof(myMessage));

			call Leds.led1Toggle();

			msg->type = CONTROL_MESSAGE;
			msg->nodeid = TOS_NODE_ID;
			msg->dv_dist = min_dist;
			msg->dest = 0;

			if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
				busy = TRUE;
			}
		}
	}

	event void TimeoutTimer.fired() {

		int i;

		SensingNeighbor = FALSE;

		// check if some neighbor is off
		for(i=1; i<=MAX_NODE_NUMBER; ++i) {
			if(dv_neighbor_t[i]) {
				dv_neighbor[i] = FALSE;
			}
		}

		// if the min_node is off, we need to compute another path
		if(dv_neighbor[min_node] == FALSE && min_dist < MAX_UINT8_INT) {

			min_node = 1;
			min_dist = MAX_UINT8_INT;

			for(i=1; i<=MAX_NODE_NUMBER; ++i) {
				if(dv_neighbor[i] && dv_dist[i] + 1 < min_dist) {
					min_dist = dv_dist[i] + 1;
					min_node = i;
				}
			}

			SendControlMessage();
		}
	}

	event void RadioControl.stopDone(error_t error) {
		// do nothing
	}

	event void SenseDataTimer.fired() {

		bool OKToSendData = FALSE;

		if(min_dist < MAX_UINT8_INT) {
			OKToSendData = TRUE;
		}

		if(!busy && OKToSendData) {
		
			myMessage* msg = (myMessage *) call Packet.getPayload(&pkt, sizeof(myMessage));
			
			call Leds.led0Toggle();

			call Temperature.read();
			call Humidity.read();
			call Light.read();

			msg->type = DATA_MESSAGE;
			msg->nodeid = TOS_NODE_ID;
			msg->temperature = temperature;
			msg->humidity = humidity;
			msg->light = light;
			msg->seq_num = seq_num;

			msg->tot_node = 1;
			msg->path[1] = TOS_NODE_ID;

			msg->dest = min_node;

			++ seq_num;

			if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
				busy = TRUE;
			}
		}
	}

	event void RadioSend.sendDone(message_t* msg, error_t error) {

		if(msg == &pkt) {
			call Leds.led0Off();
			call Leds.led1Off();
			call Leds.led2Off();

			busy = FALSE;
		}
	}

	event void Temperature.readDone(error_t error, uint16_t data) {
		temperature = data;
	}

	event void Humidity.readDone(error_t error, uint16_t data) {
		humidity = data;
	}

	event void Light.readDone(error_t error, uint16_t data) {
		light = data;
	}

	// to implement multi-hop
	event message_t* RadioReceive.receive(message_t* msg, void* payload, uint8_t len) {

		myMessage *receivedMsg = (myMessage *) payload;

		if(receivedMsg->dest != TOS_NODE_ID && receivedMsg->dest != 0) {
			return msg;
		}

		if(receivedMsg->nodeid == TOS_NODE_ID) {
			return msg;
		}

		if(len != sizeof(myMessage)) {
			return msg;
		}

		if(receivedMsg->type == SENSE_MESSAGE) {

			// receive a sense message, reply to it to show my existence and tell it my minimum path to sink
			if(!busy) {

				myMessage *toBeSentMsg = (myMessage *) call Packet.getPayload(&pkt, sizeof(myMessage));

				call Leds.led1Toggle();

				toBeSentMsg->type = REPLY_MESSAGE;
				toBeSentMsg->nodeid = TOS_NODE_ID;
				toBeSentMsg->dv_dist = min_dist;

				toBeSentMsg->dest = receivedMsg->nodeid;

				if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
					busy = TRUE;
				}
			}

		} else if(receivedMsg->type == REPLY_MESSAGE) {

			// receive a reply message, update the neighbors
			if(SensingNeighbor) {

				int i, new_min_node, new_min_dist;

				dv_neighbor_t[receivedMsg->nodeid] = FALSE;

				dv_dist[receivedMsg->nodeid] = receivedMsg->dv_dist;
				dv_neighbor[receivedMsg->nodeid] = TRUE;
				
				// recompute the shortest path to the sink
				new_min_node = 1;
				new_min_dist = MAX_UINT8_INT;
				for(i=1; i<=MAX_NODE_NUMBER; ++i) {
					if(dv_neighbor[i] && dv_dist[i] + 1 < new_min_dist) {
						new_min_dist = dv_dist[i] + 1;
						new_min_node = i;
					}
				}

				if(new_min_dist != min_dist || new_min_node != min_node) {

					min_node = new_min_node;
					min_dist = new_min_dist;

					SendControlMessage();
				}
			}

		} else if(receivedMsg->type == CONTROL_MESSAGE){

			// receive a control message, update the distance vector
			dv_dist[receivedMsg->nodeid] = receivedMsg->dv_dist;
			dv_neighbor[receivedMsg->nodeid] = TRUE;

			if(receivedMsg->dv_dist + 1 < min_dist) {

				min_dist = receivedMsg->dv_dist + 1;
				min_node = receivedMsg->nodeid;

				SendControlMessage();
			}

		} else {
			
			// relay other node's message to the sink
			if(!busy) {

				myMessage *toBeSentMsg = (myMessage *) call Packet.getPayload(&pkt, sizeof(myMessage));

				int i;

				call Leds.led2Toggle();

				toBeSentMsg->type = receivedMsg->type;
				toBeSentMsg->nodeid = receivedMsg->nodeid;
				toBeSentMsg->temperature = receivedMsg->temperature;
				toBeSentMsg->humidity = receivedMsg->humidity;
				toBeSentMsg->light = receivedMsg->light;
				toBeSentMsg->seq_num = receivedMsg->seq_num;

				toBeSentMsg->dest = min_node;
				toBeSentMsg->tot_node = receivedMsg->tot_node + 1;
				
				for(i=1; i<toBeSentMsg->tot_node; ++i) {
					toBeSentMsg->path[i] = receivedMsg->path[i];
				}
				toBeSentMsg->path[toBeSentMsg->tot_node] = TOS_NODE_ID;
		
				if((call RadioSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(myMessage))) == SUCCESS) {
					busy = TRUE;
				}
			}
		}

		return msg;
	}
}
