#include <Timer.h>
#include "myMessage.h"

configuration SenserC {

}

implementation {

		components MainC, LedsC, ActiveMessageC, PrintfC, SerialStartC;
		components new TimerMilliC() as Timer;
		components new TimerMilliC() as Timer1;
		components new TimerMilliC() as Timer2;
		components SenserP as App;
		components new SensirionSht11C() as SHT11C;
		components new HamamatsuS1087ParC() as LightC;

		// to implement multi-hop


		App.Boot -> MainC;
		App.SenseDataTimer -> Timer.Timer;
		App.SenseNeighborTimer -> Timer1.Timer;
		App.TimeoutTimer -> Timer2.Timer;
		App.Leds -> LedsC;
		App.RadioControl -> ActiveMessageC.SplitControl;
		App.Packet -> ActiveMessageC;
		App.RadioSend -> ActiveMessageC.AMSend[AM_MYMESSAGE];
		
		App.Temperature -> SHT11C.Temperature;
		App.Humidity -> SHT11C.Humidity;
		App.Light -> LightC;

		// to implement multi-hop
		App.RadioReceive -> ActiveMessageC.Receive[AM_MYMESSAGE];
}
