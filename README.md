# README #

This is my codes for the main project on course **Computer Networks**, taught by [_Prof. Yanmin Zhu_](http://www.cs.sjtu.edu.cn/~yzhu/) from SJTU. In this project, I designed and implemented a Sensor Network Communications Protocol in nesC on platform TinyOS.

# Project Report:
http://kaichun-mo.com/resume/projects/wireless.pdf

# Contact:
Webpage: http://www.kaichun-mo.com